require "#{Dir.pwd}/lib/orta_api.rb"

OrtaApi.setup do |config|
	config.user   = "el@coincycle.co"
	config.token  = "thegetdown"
	config.pass   = " ERjKVkE4RJM2diu4cWyHSZ3qSK21mLXrCFdn"
end

OrtaApi::Account.all
OrtaApi::MerchantAccount.all("16")
OrtaApi::Organization.all
OrtaApi::Account.find("14")
OrtaApi::Organization.find("5")
OrtaApi::Account.find("f57dd130e51b0133d18170188b24c56d")
OrtaApi::Organization.find("19060ed0bc4101339fe670188b24c56d")

# CREATE
params = {
    account: {
    name: "JAVA JUNCTION",
		payment_prefix: "junktion",
    alert_email: "junktion@coincycle.co"
  }
}
OrtaApi::Account.create(params)



params = {
  integration: {
    short_code: "668892",
    service_provider: "SAFARICOM",
    integration_type: "C2B",
    business_name: "ALPHABET",
    callback_urls_attributes: [{"url": "paycycle.dev/orta/ipn"}]
  }
}


org_params = {
  "organization": {
    "name": "MURANI"
  }
}
OrtaApi::Organization.create(org_params)

# "http://localhost:9292/api/organizations/bd661f80b1ea0133751a38eaa7878105/hooks"
org_hook = {
  hook: {
    name: "JAVA HOUSE C2B",
    event_type: "c2b",
    webhook_url: "http://localhost:9393/c2b/callback"
  }
}

update_hook = {
  hook: {
    name: "JAVA HOUSE"
  }
}
OrtaApi::Organization.hooks("5")
OrtaApi::Organization.create_hook("5", org_hook)
OrtaApi::Organization.update_hook("5", "e36875000e7801343eca70188b24c56d", update_hook)


# UPDATE
update = {
  account: {
    name: "JAVA EMBASSY HOUSE"
  }
}

orgg_params = {
  "organization": {
    "name": "GORE ALL"
  }
}
OrtaApi::Organization.update("19", orgg_params)
OrtaApi::Account.update("53dd81e00e1701343d0670188b24c56d", update)

# DESTROY
OrtaApi::Account.destroy("53dd81e00e1701343d0670188b24c56d")
OrtaApi::Organization.destroy("15")


paybill_params = {
  provider: "mpesa",
  paybill_type: "c2b",
  paybill_number: 711437
}
OrtaApi::Paybill.connect(paybill_params)

# 53dd81e00e1701343d0670188b24c56d
# 16


mparams={
  "merchant_account": {
    "name": "COFFEE AND ESPRESSO",
      "payment_aliases_attributes": [
        {"name": "frappe"},
        {"name": "mocha"},
        {"name": "espresso"},
        {"name": "macchiato"},
        {"name": "americano"}
      ]
    }
}
OrtaApi::MerchantAccount.create("16", mparams)

# MPESA DISBURSE
disburse_params = {
  recipient: "0700000000",
  amount: 10000,
  token: "f57dd130e51b0133d18170188b24c56d",
  transaction_id: SecureRandom.hex
}

OrtaApi::Transaction.disburse_mpesa(disburse_params)
OrtaApi::Transaction.mpesa_disburse(disburse_params)

# BANK DISBURSE
bank_disburse =
{
  amount: 1000,
  token: "19060ed0bc4101339fe670188b24c56d",
  account_name:"COINCYCLED LIMITED",
  account_number: "100300456",
  bank_name:"CHASE BANK",
  bank_code:"0908",
  conversation_id: SecureRandom.hex
}
OrtaApi::Transaction.disburse_bank(bank_disburse, "RTGS")
