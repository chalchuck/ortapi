module OrtaApi
  class Transaction < Connection
    class << self

      def disburse_mpesa(options)
        post("/mpesa/disburse/", options)
      end

      def disburse_bank(options, opt="EFT")
        post("/bank/disburse/", options.merge(disbursement_type: opt))
      end
    end
  end
end
