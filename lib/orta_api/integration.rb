module OrtaApi
	class Integration < Connection
		class << self

			def find(id, integration_id)
			  get("/organizations/#{id}/integrations/#{integration_id}")
			end

			def create(id, options)
				post("/organizations/#{id}/integrations", options)
			end

			def update(id, integration_id, options)
				put("/organizations/#{id}/integrations/#{integration_id}", options)
			end

			def destroy(id)
			  delete("/organizations/#{id}/integrations/#{integration_id}")
			end

			def list(id ,page: 1, per_page: 2)
				get("/organizations/#{id}/integrations", {page: page, per_page: per_page})
			end
		end
	end
end
