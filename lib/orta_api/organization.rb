module OrtaApi
	class Organization < Connection

		class << self

			def find(id)
			  get("/organizations/#{id}")
			end

			def create(options)
				post("/organizations/", options)
			end

			def update(id, options)
				put("/organizations/#{id}", options)
			end

			def destroy(id)
			  delete("/organizations/#{id}")
			end

			def list(page: 1, per_page: 2)
				get("/organizations", {page: page, per_page: per_page})
			end
		end
	end
end
