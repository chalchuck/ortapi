module OrtaApi
	class Configuration

		attr_accessor :user, :pass, :token

		def initialize(params={})
			@user  ||= params.dig(:user)
			@pass  ||= params.dig(:key)
			@token ||= params.dig(:token)
		end

		def header
		  {
				user: user,
				pass: pass,
				token: token,
				'Accept' => 'application/json',
				'Content-Type'=> 'application/json'
			}
		end
	end
end
