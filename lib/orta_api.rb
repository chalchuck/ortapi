require 'httparty'
require "orta_api/configuration"
require "orta_api/connection"
require "orta_api/organization"
require "orta_api/response"
require "orta_api/version"
require "orta_api/integration"
require "orta_api/transaction"

module OrtaApi
  class << self
    attr_writer :configuration

    def setup
      yield configuration
    end

    def configuration
      @configuration ||= Configuration.new
    end
    alias_method :reset, :configuration
  end
end
